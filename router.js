import Vue from 'vue';
import Router from 'vue-router';
import About from './views/About.vue';
import Features from './views/Feature.vue';
import Contact from './views/Contact.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'about',
      component: About
    },
    {
      path: '/features',
      name: 'features',
    //   meta: { layout: 'footerless' },
      component: Features
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
  ]
})