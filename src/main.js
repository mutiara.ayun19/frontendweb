import '@babel/polyfill'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import { Install as VBreakpoint } from 'vue-breakpoint-component'

Vue.use(VBreakpoint)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
